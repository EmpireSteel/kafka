from flask import render_template, redirect, request, url_for, json
from app import app
import os
import importlib
from flask_wtf import FlaskForm
from wtforms.fields.html5 import TimeField, DateField
from wtforms.fields import SubmitField
import time
from datetime import datetime



BASE_DIR = os.getcwd()
class ExampleForm(FlaskForm):
    dt1 = DateField('DatePicker1', format='%Y-%m-%d')
    dt2 = TimeField('DatePicker2')
    dt3 = DateField('DatePicker3', format='%Y-%m-%d')
    dt4 = TimeField('DatePicker4')
    submit = SubmitField('Далее')


@app.route('/')
def index():
    scripts = []
    for i in os.listdir(os.path.join(BASE_DIR, 'app/scripts')):
        if not i.startswith('__'):
            scripts.append(i.split('.')[0])
    return render_template('index.html', scripts=scripts)



@app.route('/script/<name>',methods = ['POST', 'GET'])
def run_script(name):
    script = importlib.import_module('app.scripts.' + name)
    if name.split('_')[1] == 'table':
        result = script.table()
        return render_template('table.html', data=result)
    elif name.split('_')[1] == 'graph':
        form = ExampleForm()
        if form.validate_on_submit():
            t1 = int(time.mktime(datetime.strptime(' '.join([form.dt1.data.strftime('%Y-%m-%d'), str(form.dt2.data)]), '%Y-%m-%d %H:%M:%S').timetuple()))
            t2 = int(time.mktime(datetime.strptime(' '.join([form.dt3.data.strftime('%Y-%m-%d'), str(form.dt4.data)]), '%Y-%m-%d %H:%M:%S').timetuple()))
            result = script.graph(t1, t2)
            return render_template('graph.html', data=result, form=form)
        result = script.graph()
        return render_template('graph.html', data=result, form=form)
