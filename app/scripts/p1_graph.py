import time
from random import randint


def graph(time_from=time.time() - (24*3600), time_to=time.time()):
    graph = []
    for i in range(10, -1, -1):
        graph.append((int(time.time()-3600*24*i), randint(0, 100)))
    return graph