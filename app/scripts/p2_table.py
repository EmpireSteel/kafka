import time
from random import randint


def table(time_from=time.time() - (24*3600), time_to=time.time()):
    data = {}
    data['tittles'] = ['param1', 'param2', 'param3']
    data['data'] = [[randint(0, 10), randint(0, 100), randint(100,1000)] for i in range(100)]
    return data
